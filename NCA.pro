TEMPLATE = subdirs

SUBDIRS = libNCACore \
          NCAGui \
          NCATests \
          NCACommandLine

contains(QT_VERSION, ^5\\.[0-1]\\..*) {
    message("Cannot build NCACommandLine with Qt version $${QT_VERSION}.")
    message("Use at least Qt 5.2.")
    SUBDIRS -= NCACommandLine
}

CONFIG += ordered c++11

OTHER_FILES = config.xml \
              test_config.xml \
              .gitlab-ci.yml \
              generateDebPackage.py \

