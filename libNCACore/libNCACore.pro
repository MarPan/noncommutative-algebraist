QT       -= gui

CONFIG   += dll shared

TEMPLATE = lib

DEFINES	+= __DLL_CLASS_LINKAGE_SPECIFICATION__ NCA_LIBRARY

include(literals/literals.pri)
include(config/config.pri)
include(core/core.pri)

unix {
  TARGET = ../out/NCACore
  target.path = /usr/lib
  config.files = ../config.xml ../test_config.xml
  config.path = /usr/share/nca
  INSTALLS += target config
}
win32 {
  TARGET = ../../out/NCACore
  TARGET_EXT = .dll
}
win64 {
  TARGET = ../../out/NCACore
  TARGET_EXT = .dll
}

OBJECTS_DIR = ../intermediate/lib/
MOC_DIR = ../intermediate/lib/

SOURCES += \
    logger.cpp \
    libNCACore_global.cpp

HEADERS += \
    libNCACore_global.h \
    logger.h

VERSION = 1.3.8
# Define the preprocessor macro to get the application version in our application.
DEFINES += APP_VERSION=\\\"$$VERSION\\\"
