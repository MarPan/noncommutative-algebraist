#include "groupgraph.h"

GroupLabel::GroupLabel(const QString name): _groupName(name)
  {
  }

bool GroupLabel::addLabel(const QString label)
  {
  if (!_labels.contains(label))
    {
    _labels.append(label);
    return true;
    }
  else
    {
    return false;
    }
  }

bool GroupLabel::removeLabel(const QString label)
  {
  return _labels.removeOne(label);
  }

QString GroupLabel::getName() const
  {
  return _groupName;
  }

QStringList GroupLabel::getLabels() const
  {
  return _labels;
  }

int GroupLabel::size() const
  {
  return _labels.size();
  }

inline bool GroupLabel::operator< (const GroupLabel& rhs) const
  {
  return _groupName < rhs._groupName;
  }

inline bool GroupLabel::operator> (const GroupLabel& rhs) const
  {
  return _groupName > rhs._groupName;
  }

//------------------------GROUPGRAPH------------------------------------
GroupGraph::GroupGraph()
  {
  }

bool GroupGraph::contains(QString groupName)
  {
    foreach (GroupLabel gl, _labelsGraph.keys())
      {
      if (gl.getName() == groupName)
        return true;
      }
    return false;
  }

const GroupLabel* GroupGraph::getGroupLabel(QString groupName)
  {
  QMapIterator<GroupLabel, QList <GroupLabel*> > i(_labelsGraph);
  while (i.hasNext())
    {
    i.next();
    if (i.key().getName() == groupName)
      return &i.key();
    }
  return nullptr;
  }

bool GroupGraph::addSubgroup(QString groupName, QString subgroupName)
  {
  const GroupLabel* gl = getGroupLabel(groupName);
  const GroupLabel* sgl = getGroupLabel(subgroupName);
  if (gl != nullptr && sgl != nullptr)
    _labelsGraph[ *gl ].append(const_cast<GroupLabel*>(sgl));
  return true;
  }

bool GroupGraph::addGroup(GroupLabel& gl)
  {
  QList <GroupLabel*> list;
  _labelsGraph.insert(gl, list);
  return true;
  }

bool GroupGraph::removeSubgroup(QString groupName, QString subgroupName)
  {
  const GroupLabel *glname = getGroupLabel(groupName);
  if (glname != nullptr)
  {
    foreach (GroupLabel* gl, _labelsGraph[*glname])
      {
      if ((gl->getName() == subgroupName) && (subgroupName != groupName) )
        {
          _labelsGraph[*glname].removeOne(gl);
          return true;
        }
      }
  }
  return false;
  }

bool GroupGraph::removeGroup(GroupLabel& gl)
  {
  //delete appropriate values from map
  foreach (QList <GroupLabel*> list, _labelsGraph)
    {
    list.removeAll(&gl);
    }

  //delete key
  if (_labelsGraph.remove(gl) > 0 )
    return true;
  return false;
  }

int GroupGraph::size() const
  {
  return _labelsGraph.size();
  }

void GroupGraph::clear()
  {
  // we don't need to get creative: all pointers are pointing to one of the keys anyway.
  _labelsGraph.clear();
  }

void GroupGraph::print()
  {
  qWarning() << "_labelsGroup";
  QStringList subgroups;
  QMapIterator<GroupLabel, QList <GroupLabel*> > i(_labelsGraph);
  while (i.hasNext())
    {
    i.next();
    qWarning() << "Group: " <<i.key().getName();
    subgroups.clear();
    foreach (GroupLabel* gl, i.value())
      {
      subgroups.append(gl->getName());
      }
    qWarning() << "Subgroup: "<< subgroups;
    }
  }

