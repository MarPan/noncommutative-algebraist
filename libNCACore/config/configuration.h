﻿#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#include <QXmlStreamReader>
#include <QVector>
#include <QList>
#include "core/translator.h"
#include "config/groupgraph.h"
#include "config/macrorule.h"
/*
 * This class should be responsible for reading and writing XML files.
 * I think it might be a good place to store our rules too.
 * Also, all other stuff: list of operators, groups and so on probably belong here.
 */
class NCA_EXPORT Configuration
  {
  friend class ConfigurationTest;
public:
  Configuration(Translator &translator);

  bool loadXML(QString file);
  bool treatLabelsAsNumbers() { return _treatLabelsAsNumbers; }
  QList<Rule> & rules() { return _rules; }
  QList<Rule> & generatedRules() { return _generatedRules; }
  QList<MacroRule> & macroRules() { return _macroRules; }

  QMap<int, Position> updateMacroRules(const Expression &exp);

private:
  bool parseRules(QXmlStreamReader& xml);
  bool parseCondition(QXmlStreamReader& xml, Rule &rule);
  bool parseGroups(QXmlStreamReader& xml);
  bool parseRegisteredNames(QXmlStreamReader& xml);
  QString parseData(QXmlStreamReader& xml);
  bool parseLabelsPriorities(QXmlStreamReader& xml);
  bool parseMacro(QXmlStreamReader& xml, Rule &rule);

  GroupLabel* findGroupLabel(QString name) const;
  void clear();
  QString tokenTypeToString(QXmlStreamReader::TokenType tokenType);
  void xmlWarning(QXmlStreamReader& xml, QString msg);

  QList<Rule> _rules;
  QList<MacroRule> _macroRules;
  QList<Rule> _generatedRules; // container for rules generated from macros

  GroupGraph _gGraph; //graph of groups
  QStringList _labelsPriorities;
  Translator &_translator;
  bool _treatLabelsAsNumbers;
};

#endif // CONFIGURATION_H
