#ifndef MACRORULE_H
#define MACRORULE_H

#include <QList>
#include "rule.h"
#include "core/translator.h"

struct MacroVariable
  {
  MacroVariable(QString n, QString minV, QString maxV) :
    name(n), minValue(minV), maxValue(maxV) {}
  QString name;
  QString minValue;
  QString maxValue;
  };

class NCA_EXPORT MacroRule
  {  
  friend class MacroRuleTest;
public:
  enum MacroOperation
    {
    MO_SUM,
    MO_PRODUCT
    };

  MacroRule(const Expression &pattern, const Expression &replacement,
            const QString& macroReplacement, MacroOperation op,
            QList<MacroVariable> vars);
  MacroRule(const Rule &rule,
            const QString& macroReplacement, MacroOperation op,
            QList<MacroVariable> vars);

  MacroRule& operator= (const MacroRule& rhs); // needed by QList to store us
  QList<Rule> generateRules(Expression const &exp, Translator const & translator);

  // so far getRule() is only used in configuration to get to
  // Rule::findPatternInExp() and
  // Rule::key()
  const Rule & getRule() { return _rule; }

private:
  QList<int> getNextCombination(QList< QPair<int, int> > vars, QList<int> prevValues);

  Rule _rule;
  QString _macroReplacement;
  MacroOperation _operation;
  QList<MacroVariable> _variables;
  };


#endif // MACRORULE_H
