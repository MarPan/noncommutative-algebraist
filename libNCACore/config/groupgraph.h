#ifndef GROUPLABEL_H
#define GROUPLABEL_H

#include <QStringList>
#include <QMap>
#include <QDebug>
#include "libNCACore_global.h"

class NCA_EXPORT GroupLabel
{
public:
    GroupLabel(const QString name);
    bool addLabel(const QString label); // returns false if not added, i. e label exist
    bool removeLabel(const QString label); // returns true if removed
    QString getName() const;
    QStringList getLabels() const;
    int size() const; // return number of labels
    inline bool operator< (const GroupLabel& rhs) const;
    inline bool operator> (const GroupLabel& rhs) const;
private:
    QString _groupName;
    QStringList _labels;
};

class NCA_EXPORT GroupGraph
{
public:
    GroupGraph(QMap <QString, QList <GroupLabel*> > graph);
    GroupGraph();
    bool addSubgroup(QString groupName, QString SubgroupName); // add group to correlation list
    bool addGroup(GroupLabel& gl); // add new node
    bool removeSubgroup(QString groupName, QString SubgroupName); // remove subgroupName from groupName
    bool removeGroup(GroupLabel& gl);
    bool contains(QString groupName); // checks if graph contains this group
    const GroupLabel* getGroupLabel(QString groupName);
    int size() const;
    void clear();
    void print();
private:
    QMap <GroupLabel, QList <GroupLabel*> > _labelsGraph; // this map contains list of subgroups assigned to groupLabel. Pointer inside Qlist indicate to first argment of map.
};

#endif // GROUPLABEL_H
