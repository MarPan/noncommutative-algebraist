﻿#include <QStringList>
#include <QDebug>
#include "delta.h"

Delta::Delta(const QString& text, const QString& arg1, const QString& arg2)
  :Function(Word::WT_DELTA, text)
  {
  _labels.clear();

  if ((arg1 < arg2) || arg1.startsWith("%") || arg2.startsWith("%"))
    _labels << arg1 << arg2;
  else
    _labels << arg2 << arg1;
  }

Word *Delta::createCopy() const
  {
  return new Delta(*this);
  }

void Delta::setLabels(const QStringList& args)
  {
  if (args.size() == 2)
    {
    Word::setLabels(args);
     _labels.sort();
    }
  else
    qWarning() << Q_FUNC_INFO << "Tried to set invalid number of argument for delta: " << args.size();
  }

void Delta::setLabel(const int index, const QString &text)
  {
  if ( (index < getLabels().size()) && (index >= 0))
    {
    Word::setLabel(index, text);
     _labels.sort();
    }
  else
    qWarning() << Q_FUNC_INFO << "Tried to set label at invalid index for delta: " << index;
  }
