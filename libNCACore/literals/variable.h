#ifndef VARIABLE_H
#define VARIABLE_H

#include "word.h"

class NCA_EXPORT Variable : public Word
  {
  public:
    Variable(const QString& name);
    Word *createCopy() const;
  };

#endif // VARIABLE_H
