#ifndef PRODUCT_H
#define PRODUCT_H

#include "word.h"

class NCA_EXPORT Product : public Word
  {
  public:
    Product(const QString& text, QStringList args);
    Word *createCopy() const;
  protected:
    Product(Word::Type wt, const QString& text);
  };

#endif // PRODUCT_H
