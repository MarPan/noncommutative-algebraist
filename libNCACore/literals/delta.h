﻿#ifndef DELTA_H
#define DELTA_H

#include "function.h"

class NCA_EXPORT Delta : public Function
  {
  public:
    Delta(const QString& text = "", const QString& arg1 = "", const QString& arg2 = "");

    // from Word
    void setLabels(const QStringList& args) override;
    void setLabel(const int index, const QString& text) override;
    Word *createCopy() const override;
  };

#endif // DELTA_H
