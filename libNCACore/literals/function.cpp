#include "function.h"

Function::Function(const QString& text, QStringList args)
  : Word(WT_FUNCTION, text)
  {
  setLabels(args);
  }

Function::Function(Word::Type wt, const QString& text)
  : Word(wt, text)
  {
  }

Word *Function::createCopy() const
  {
  return new Function(*this);
  }
