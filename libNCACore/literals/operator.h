﻿#ifndef OPERATOR_H
#define OPERATOR_H

#include <QString>
#include "word.h"

class NCA_EXPORT Operator : public Word
  {
  public:
    Operator(const QString& text, const QString& label);
    Operator(const QString& text, const QStringList& labels);
    ~Operator();
    Operator(const Operator &);
    Operator& operator=(const Operator &rhs);

    Word *createCopy() const;
  };

#endif // OPERATOR_H
