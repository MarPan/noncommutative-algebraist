#ifndef SUM_H
#define SUM_H

#include "word.h"
#include "../core/sentence.h"

// SUM has a textRepresentation (right now it's hardcoded as \SUM)
// It uses labels for storing information about variables
// It contains a Sentence which is independent from the rest of Sentence it resides in.
class NCA_EXPORT Sum : public Word, public Sentence
  {
  public:
    Sum(const QString& text, QStringList args);
    Word *createCopy() const override;

    bool mergeWithInnerSum();
  };

#endif // SUM_H
