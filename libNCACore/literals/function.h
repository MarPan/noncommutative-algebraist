#ifndef FUNCTION_H
#define FUNCTION_H

#include "word.h"

class NCA_EXPORT Function : public Word
  {
  public:
    Function(const QString& text, QStringList args);
    Word *createCopy() const;
  protected:
    Function(Word::Type wt, const QString& text);
  };

#endif // FUNCTION_H
