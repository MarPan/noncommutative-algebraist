﻿#include <qstringlist.h>
#include <qdebug.h>
#include "word.h"

Word::Word(Word::Type ot, QString text)
  :_type(ot), _textRepresentation(text)
  {   }

Word::Word(const Word& other)
  {
  _type = other.getType();  
  setLabels(other.getLabels());
  _textRepresentation = other._textRepresentation;
  }

Word& Word::operator=(const Word &rhs)
  {
  if (this == &rhs)
    {
    return *this;
    }
  _type = rhs._type;
  setLabels(rhs.getLabels());
  _textRepresentation = rhs._textRepresentation;
  return *this;
  }

/*
 * Two words are equal if: type and labels are the same (labels should be in the same order)
 */
bool Word::operator== (const Word &other) const
  {
  return (other.getType() == this->getType() && (other.getLabels() == this->getLabels() )&& (other.getText() == this->getText()));
  }

bool Word::operator!= (const Word &other) const
  {
  return !(*this == other);
  }

void Word::setType(Word::Type type)
  { _type = type; }
Word::Type Word::getType() const
  { return _type;  }

/*virtual*/ void Word::setLabels(const QStringList& args)
  {
  _labels = args;
  }
/*virtual*/ QStringList Word::getLabels() const
  {
  return _labels;
  }

/* This function is used to replace labels of format %1, %2, %1.0, %1.2 etc.
 * Those could be read from the config file (or generated from a macro rule).
 * I assume that
 */
/*virtual*/ void Word::applyLabelDictionary(const Dictionary &dictionary)
  {
  QStringList labels = _labels;

  // if we only have one label (e.g. %1)
  if (1 == _labels.size())
    {
    if (dictionary.contains(_labels[0]))
      labels = dictionary.value(labels[0]);
    }
  else if (0 < labels.size()) // if we have multiple labels (e.g. {%1 %2})
    {
    for (int k = 0; k < labels.size(); k++)
      {
      if (dictionary.contains(_labels[k]))
        {
        QStringList dictionaryValue = dictionary.value(labels[k]);
        if (dictionaryValue.size() > 1)
          qWarning() << "Tried to assign multiple values to a label, which should be singular given the context " << _labels.join(' ');
        else if (dictionaryValue.size() == 1)
          labels[k] = dictionaryValue[0];
        else
          qWarning() << "Empty string list in the dictionary";
        }
      }
    }
  setLabels(labels);
  }

/*virtual*/ void Word::setLabel(const int index, const QString &text)
  {
  if (index < _labels.size() && index >= 0)
    _labels[index] = text;
  }

/*
 * compares words by name. If name is the same, compares by labels
 */
/*virtual*/ bool Word::isGreaterThan(const Word &other)
  {
  bool bRet = true;
  if (0 != getText().compare( other.getText()))  // names of words are different
    bRet = getText().compare( other.getText()) > 0;
  else // names are the same, so we have to check the labels
    {
    bool labelsWereDifferent = false;
    for (int i = 0; i < getLabels().size(); i++)
      {
      if (i >= other.getLabels().size())
        break;
      if (_labels.at(i).compare(other.getLabels().at(i)) != 0)
        {
        bRet = _labels.at(i).compare(other.getLabels().at(i)) > 0;
        labelsWereDifferent = true;
        break;
        }
      }
    if (!labelsWereDifferent)
      bRet = _labels.size() > other.getLabels().size();
    }
  return bRet;
  }
