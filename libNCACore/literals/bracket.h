#ifndef BRACKET_H
#define BRACKET_H

#include "word.h"
#include "core/expression.h"

class NCA_EXPORT Bracket : public Word, public Expression
  {
  public:
    Bracket();
    Bracket(const Expression & other);
    Bracket(const Bracket & other);
    Bracket& operator=(const Bracket &rhs);
    ~Bracket();


    void applyLabelDictionary(const Dictionary& dictionary) override;

    Word *createCopy() const override;
  };
#endif // BRACKET_H
