#include "product.h"

Product::Product(const QString& text, QStringList args)
  : Word(WT_PRODUCT, text)
  {
  setLabels(args);
  }

Product::Product(Word::Type wt, const QString& text)
  : Word(wt, text)
  {
  }

Word *Product::createCopy() const
  {
  return new Product(*this);
  }
