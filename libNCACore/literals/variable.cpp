#include "variable.h"


Variable::Variable(const QString& name)
  : Word(Word::WT_VARIABLE, name)
  {
  }

Word *Variable::createCopy() const
  {
  return new Variable(*this);
  }
