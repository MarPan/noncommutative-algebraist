﻿#include <qstringlist.h>
#include <qdebug.h>
#include "operator.h"

Operator::Operator(const QString& text, const QString& label)
  : Word(Word::WT_OPERATOR, text)
  {
  _labels << label;
  }
Operator::Operator(const QString& text, const QStringList& labels)
  : Word(Word::WT_OPERATOR, text)
  {
  _labels = labels;
  }
Operator::~Operator()
  {
  //qDebug() << "Destructor inside Operator. My address is " << this;
  }

Operator::Operator(const Operator &other)
  :Word(other)
  {  }

Operator& Operator::operator=(const Operator &rhs)
  {
  if (this == &rhs)
    {
    return *this;
    }
  _labels = rhs._labels;
  _type = rhs._type;
  return *this;
  }

Word *Operator::createCopy() const
  {
  return new Operator(*this);
  }
