#include <QDebug>
#include "bracket.h"
#include "core/expression.h"

Bracket::Bracket()
  : Word(Word::WT_BRACKET)
  {  
  }

Bracket::Bracket(const Expression & other)
  : Word(Word::WT_BRACKET),
    Expression(other)
  {
  }

Bracket::Bracket(const Bracket & other)
  : Word(other), Expression(other)
  {  
  }

Bracket& Bracket::operator=(const Bracket &rhs)
  {
  if (this == &rhs)
    return *this;
  _type = rhs._type;
  _labels = rhs._labels;
  return *this;
  }

Word *Bracket::createCopy() const
  {
  return new Bracket(*this);
  }

Bracket::~Bracket()
  {
  }

void Bracket::applyLabelDictionary(const Dictionary& dictionary)
  {
  Expression::applyLabelDictionary(dictionary);
  }
