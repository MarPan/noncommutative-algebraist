#include "sum.h"

Sum::Sum(const QString& text, QStringList args)
  : Word(WT_SUM, text)
  {
  setLabels(args);
  }

Word* Sum::createCopy() const
  {
  return new Sum(*this);
  }


bool Sum::mergeWithInnerSum()
  {
  bool wasIModified = false;

  // TODO: CODE DUPLICATION with a loop in Sentence::mergeInfiniteSums
  Word * firstSum = nullptr;
  int i = 0;
  for (; i < this->size(); ++i)
    {
    Word * const word = this->at(i);
    if (word->getType() == Word::WT_SUM)
      {
      firstSum = word;
      break;
      }
    }

  if (firstSum != nullptr)
    {
    Sum* otherSum = static_cast<Sum*>(firstSum);

    // merge labels from other sum with ours.
    // ignore duplicates, append all others
    for (const QString & otherSumLabel : otherSum->getLabels())
      if (false == _labels.contains(otherSumLabel))
        _labels.append(otherSum->getLabels());

    this->removeAt(i);

    for (int j = otherSum->size() -1 ; j >= 0; j--)
      {
      if (wasIModified == false)
        wasIModified = true;
      this->insert(i, otherSum->at(j)->createCopy());
      }

    // we should delete the Sum object, but the destructor would call delete on every Word
    // from it. That's why we need to copy all the words.
    // TODO: This would be fixed if we stored shared_ptrs of Words instead of raw pointers.
    delete firstSum;

    }
  return wasIModified;
  }

