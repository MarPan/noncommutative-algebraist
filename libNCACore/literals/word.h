﻿#ifndef WORD_H
#define WORD_H

#include <qstringlist.h>
#include <QMetaType>
#include "libNCACore_global.h"

using Dictionary = QMap<QString, QStringList>;

class NCA_EXPORT Word
  {
  public:
    enum Type { WT_OPERATOR = 1,
                WT_DELTA,
                WT_BRACKET,
                WT_VARIABLE,
                WT_FUNCTION,
                WT_PRODUCT,
                WT_SUM,
                WT_INVALID = 0 };

    Word(Word::Type ot = Word::WT_INVALID, QString text = "");

    // we need copy constructor and assignment operator, because of setLabels:
    //   some types have some limitation of their resources and incorporate logic into this func.
    Word(const Word&);
    Word& operator=(const Word &rhs);
    bool operator==(const Word &other) const;
    bool operator!=(const Word &other) const;

    virtual ~Word() {}
    virtual Word *createCopy() const = 0;
    virtual void setLabels(const QStringList& args);
    virtual void setLabel(const int index, const QString& text);
    virtual QStringList getLabels() const;
    virtual bool isGreaterThan(const Word &other);
    virtual void applyLabelDictionary(const Dictionary& dictionary);

    Word::Type getType() const;
    void setText(QString s) { _textRepresentation = s; }
    QString getText() const { return _textRepresentation; }
  protected:
    void setType(Word::Type);
    Word::Type _type;
    QStringList _labels;

    // This  lets us distinguish  between different word types
    // but also between different variants of the type (ex. operator a_i vs operator b_i,
    // or function foo() vs function bar(). This is primarily used in rule-matching, because
    // we don't care about labels at that point.
    QString _textRepresentation;
  };


Q_DECLARE_METATYPE(Word::Type)

#endif // WORD_H
