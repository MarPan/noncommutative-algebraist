#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include "libNCACore_global.h"

class NCA_EXPORT Logger : public QObject
  {
  Q_OBJECT
public:
  static Logger& instance()
    {
    static Logger staticLog;
    return staticLog;
    }

  void logToStdout();
  void logToFile(QString name);
  static void log(QtMsgType type, const QMessageLogContext &context, const QString &msg);

signals:
  void error(QString msg);

private:
  Logger();
  ~Logger();
  static FILE * stream;

signals:

public slots:
  };
#endif // LOGGER_H
