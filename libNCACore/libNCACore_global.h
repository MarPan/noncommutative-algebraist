﻿#ifndef LIBNCACORE_GLOBAL_H
#define LIBNCACORE_GLOBAL_H

#include <QtCore/qglobal.h>
#include <math.h>

#if defined(NCA_LIBRARY)
#  define NCA_EXPORT Q_DECL_EXPORT
#else
#  define NCA_EXPORT Q_DECL_IMPORT
#endif

// This WILL cause problems
// http://stackoverflow.com/questions/14297515/calculating-greatest-common-divisor
// but for now I don't know any alternatives :)
double inline gcd(double a, double b)
  {
  if (a < 0)
    a = -a;
  if (b < 0)
    b = -b;

  if (a > b)
    {
    if(b == 0)
      return a;
    else
      return gcd(b, fmod(a,b));
    }
  else
    {
    if (a == 0)
      return b;
    else
      return gcd(fmod(b,a), a);
    }
  }


// https://stackoverflow.com/questions/9329406/evaluating-arithmetic-expressions-from-string-in-c

struct TinySolver
{
public:
    static int solve(const char * str)
    {
        expressionToParse = str;
        return expression();
    }

private:
    static const char * expressionToParse;

    static char peek()
    {
        return *expressionToParse;
    }

    static char get()
    {
        return *expressionToParse++;
    }

    static int number()
    {
        int result = get() - '0';
        while (peek() >= '0' && peek() <= '9')
        {
            result = 10*result + get() - '0';
        }
        return result;
    }

    static int factor()
    {
        if (peek() >= '0' && peek() <= '9')
            return number();
        else if (peek() == '(')
        {
            get(); // '('
            int result = expression();
            get(); // ')'
            return result;
        }
        else if (peek() == '-')
        {
            get();
            return -factor();
        }
        return 0; // error
    }

    static int term()
    {
        int result = factor();
        while (peek() == '*' || peek() == '/')
            if (get() == '*')
                result *= factor();
            else
                result /= factor();
        return result;
    }

    static int expression()
    {
        int result = term();
        while (peek() == '+' || peek() == '-')
            if (get() == '+')
                result += term();
            else
                result -= term();
        return result;
    }
};


#endif // LIBNCACORE_GLOBAL_H
