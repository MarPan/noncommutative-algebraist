﻿#ifndef SENTENCE_H
#define SENTENCE_H

#include <QList>
#include "literals/word.h"

class NCA_EXPORT Sentence : public QList<Word*>
  {
  friend class SentenceTest;
  public:
    Sentence();
    ~Sentence();
    Sentence(Sentence && s);
    Sentence(const Sentence&);
    Sentence& operator=(const Sentence &rhs);
    Sentence& operator=(Sentence &&rhs);

    bool operator==(const Sentence &other) const; // compare with coefficient
    bool operator!=(const Sentence &other) const;

    // consts
    bool compareWithoutCoefficient(const Sentence &other) const; // returns true if sentences are the same
    int getCommonPart(const Sentence pattern, Sentence &commonPart) const;
    Sentence getSubsentenceOfType(Word::Type type) const;    
    bool isGreaterThan(const Sentence& other) const;
    double getCoefficient() const;
    int countWordType(Word::Type type) const;

    //
    void setCoefficient(double);
    void appendWordsFromSentence(Sentence const& other, int startingWord, int count);
    void concatenate(const Sentence &other);
    int removeWords(int startingWord, int count);
    void cleanup();

    // simplifiers
    bool doDeltaTrick(); // change of labels
    bool groupTypes();
    bool simplifyInfiniteSums();

  private:
    bool sortType(const int index, const int count, Word::Type type);
    int firstOccurrenceOfType(Word::Type type) const;
    double _coefficient;
  };

#endif // SENTENCE_H
