﻿#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <QStringList>
#include <QHash>
#include "literals/word.h"
#include "expression.h"

class Expression;
class Bracket;
class Sum;

class NCA_EXPORT Translator
  {
public:
    Translator();

    QString expToString( const Expression &) const;
    QString sentenceToString( const Sentence &) const;

    bool stringToExp( const QString &, Expression &exp ) const;
    bool stringToSentence( const QString &, Sentence &sen ) const;

    void registerName(QString sName, Word::Type type);
    int clearRegisteredNames();
    void setDefaultRegisteredNames();

  private:
    enum ParserError {
      PE_BRACKET = 1,
      PE_DELTA_LABELS,
      PE_FUNCTION_ARGUMENTS,
      PE_OPERATOR_LABEL,
      PE_COEFFICIENT,
      PE_GENERAL_ERROR,
      PE_SUM_LABEL,
      PE_NO_ERROR = 0
    };

    QHash<QString,Word::Type> _registeredNames;

    int parseCoefficient(int i, const QString &str, double &coeff) const;
    int parseBracket(int i, const QString &str, Bracket &bracket) const;
    int parseArguments(int i, const QString &str, QStringList &args) const;
    int parseSum(int i, const QString &str, Sum &sum) const;
  };

#endif // TRANSLATOR_H
