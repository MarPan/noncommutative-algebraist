﻿/*
 * WARNIG: This class uses (extensively!) Qt's qDebug()'s, qWarning()'s, qWarning()'s
 * and qFatal()'s by installing it's own Message Handler.
 * 
 * If you want to supress logging, you should install your own message handler that will 
 * ignore all incoming data (see qInstallMsgHandler).  
 * 
 */

#ifndef EXPRESSIONSIMPLIFIER_H
#define EXPRESSIONSIMPLIFIER_H

#include <QList>
#include <QString>
#include <QMap>
#include <QPair>
#include "expression.h"
#include "translator.h"
#include "config/configuration.h"
//#include <vld.h>

class NCA_EXPORT ExpressionSimplifier : public QObject
  {  
  friend class ExpressionSimplifierTest;
  Q_OBJECT
  public:
    ExpressionSimplifier(bool fileLogging = true, const QString &configPath = "config.xml");
    ~ExpressionSimplifier();

    QStringList simplify();                                // upraszcza exp, zapisując poszczególne kroki do steps, na koniec stosując appends()
    bool doOneStep();

    // TODO: should those be private?
    bool multiplyAllBrackets();
    bool simplifyNumberDeltas();
    bool simplifyDeltas(bool numbers = true);
    bool sameSentencesArithmetic();
    bool mergeInfiniteSums();
    bool factorOut();

    void setExpFromString(const QString& input);    // używając translatora buduje z inputu Expression i zapisuje do exp

    QString getExp();
    QString expToString();                          // zwraca exp w postaci stringa

    bool loadConfig(QString path);
    QString getVersion();

  signals:
    void error(QString);

  private:
    Expression _exp;                         // przechowuje upraszczane wyrażenie
    QList< QList<Expression> > stepsList;   // przechowuje listy przekształceń poszczególnych Sentenców (uwaga: upraszczanie jednego Sentence'a -> jedno równanie)
    Translator _translator;                  // zawiera i obsługuje mapy do przekładania na postać jawnego tekstu lub LaTeXową.

    QScopedPointer<Configuration> _config;

    bool findAndApply(const QList<Rule> &rules); // finds the first Rule that can be applied and applies it.

    Q_DISABLE_COPY(ExpressionSimplifier)
  };

#endif // EXPRESSIONSIMPLIFIER_H
