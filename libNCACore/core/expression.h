﻿#ifndef EQUATION_H
#define EQUATION_H

#include <vector>
#include "sentence.h"

class NCA_EXPORT Expression : public std::vector<Sentence>
  {
  friend class ExpressionTest;
  public:
    Expression();
    bool operator== (const Expression &other) const;

    //Word &wordAt(QPair<size_t, size_t> position);
    // params are that way, because Expression is a vector and expects unsigned indexes
    // and Sentence is a QList and expects signed indexes.
    Word const &wordAt(QPair<size_t, int> position) const;

    void concatenate(Expression* other);   /*< appends other expression after this one, concatenating the two of them.*/
    void subExpression(size_t startingSentence, int startingWord, int wordCount, Expression &sub) const;

    void copyExpressionToMe(Expression *newE, int i); /*< inserts newS at specified index */

    bool groupTypes();
    bool doDeltaTricks();
    bool sameSentencesArithmetic();
    bool multiplyAllBrackets();

    void removeWords(size_t startingSentence, int startingWord, int wordCount);
    void insertIntoSentence(size_t startingSentence, int startingWord, const Expression &expression); /*< inserts expression at exact specified location*/
    void applyLabelDictionary(const QMap<QString, QStringList> &dict); /*< Replaces labels according to dictionary */

    int countWordType(Word::Type type) const;
    int countWords() const;
    void multiplyWith(Expression * other, Expression &result) const; /*< performs this*other. Order matters! Returns result of multiplication in a new object. */
    void multiplyByNumber(double n);

    void sort(); // sorts sentences in expression

    bool factorOutWhatYouCan();

  private:
    bool factorOutType(Word::Type type);
    bool factorOutCommonSentence();

    static bool sentenceLessThan(const Sentence &s1, const Sentence &s2);
  };

Q_DECLARE_METATYPE(Expression)

#endif // EQUATION_H
