#include <QTextStream>
#include <QDebug>
#include <QFile>
#include <iostream>


#include "mainclass.h"

MainClass::MainClass()
  {
  bool fileLogging = true;
  QString defaultConfigPath = "";

  #if defined(DEBUG)
    defaultConfigPath = "../config.xml";
  #else
    defaultConfigPath = "config.xml";
  #endif

  _simplifier = new ExpressionSimplifier(fileLogging, defaultConfigPath);
  }

void MainClass::setCommandsAndRun(QStringList args)
  {
  parser.setApplicationDescription("Example: ./NCACommandLine -s \"a_x b_y a_z\" \nPlease remember to add quotes to expression!");
  parser.addHelpOption();
  parser.addVersionOption();

  QCommandLineOption verbose(QStringList() << "e" << "eloquent", QCoreApplication::translate("english", "Show progress during simplifying"));
  parser.addOption(verbose);

  QCommandLineOption simplify(QStringList() << "s" << "simplify",
          QCoreApplication::translate("english", "simplify expression"),
          QCoreApplication::translate("english", "expression"));
  parser.addOption(simplify);

  QCommandLineOption writeToFile(QStringList() << "w" << "write-to-file",
          QCoreApplication::translate("english", "Save steps to file"),
          QCoreApplication::translate("english", "file"));
  parser.addOption(writeToFile);

  QCommandLineOption readFile(QStringList() << "f" << "file",
          QCoreApplication::translate("english", "Read from file"),
          QCoreApplication::translate("english", "file"));
  parser.addOption(readFile);

  parser.process(args);

  QString expression = parser.value(simplify);
  verboseMode = parser.isSet(verbose);
  writeToFileMode = parser.isSet(writeToFile);
  launcher(expression, parser.value(writeToFile), parser.value(readFile));
  }

int MainClass::launcher(QString expression, QString pathTo, QString pathFrom)
  {
  //clear file pathTo if exist
  if (!pathTo.isEmpty())
    {
    QFile data(pathTo);
    data.resize(0);
    }

  //launch Simplifier
  if (!expression.isEmpty())
    {
      runSimplifier(expression, pathTo);
      return 0;
    }

  if (!pathFrom.isEmpty()) //read from file
    {
    QFile file(pathFrom);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
      {
      qWarning() << "Opening file " << pathFrom << "failed";
      return 1;
      }
    QTextStream stream(&file);
    QString line = stream.readLine();
    while (!line.isNull())
      {
      runSimplifier(line, pathTo);
      line = stream.readLine();
      }
    }
  else //read from stdin
    {
    QTextStream stream(stdin);
    QString line = stream.readLine();
    while (!line.isNull() && !line.isEmpty() )
      {
      runSimplifier(line, pathTo);
      line = stream.readLine();
      }
    }
  return 0;
}

void MainClass::runSimplifier(QString expression, QString pathTo)
  {
  _simplifier->setExpFromString(expression.trimmed());
  simplifiedExpression = _simplifier->simplify();
  printResponse(expression, pathTo);
  }

int MainClass::printResponse(QString originalExpression, QString pathTo)
{
  QTextStream qout(stdout);//by default print to stdout
  QFile data(pathTo);

  if (!pathTo.isEmpty()) //print to file
    {
    if (data.open(QFile::WriteOnly | QFile::Append))
      {
      qout.setDevice(&data);
      }
    else
      {
      qWarning() << "Opening file " << pathTo << "failed. Soultion will not be saved.";
      return -1;
      }
    }

  if (verboseMode)
    {
    qout << "Original expression: " << originalExpression << endl;
    for (int i = 0; i < simplifiedExpression.size(); i++)
      qout << i << ": " << simplifiedExpression[i] << endl;
      qout << endl;
    }
  else
    {
    if (!writeToFileMode)
      qout << "Solution: ";
    qout << simplifiedExpression[simplifiedExpression.size()-1] << endl;
    if (!writeToFileMode)
      qout << endl;
    }
  return 0;
}
