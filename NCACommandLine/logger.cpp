﻿#include "logger.h"
#include <QDebug>
#include <QStringList>

Logger::Logger(QObject *parent) :
    QObject(parent)
  {
  }

void Logger::message(QStringList& m)
  {
  qDebug() << "LOGGER:" <<  m;
  }

void Logger::message(QString& m)
  {
  qDebug() << "LOGGER:" << m;
  }


