#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QCommandLineParser>
#include "core/expressionsimplifier.h"

class MainClass
  {
public:
  MainClass();
  void setCommandsAndRun(QStringList args);
private:
  ExpressionSimplifier *_simplifier;
  QStringList simplifiedExpression;
  QCommandLineParser parser;

  bool verboseMode; //prints steps to achieve result
  bool writeToFileMode; //prints steps to achieve result

  int printResponse(QString originalExpression, QString pathTo); //print output (to file or console)
  int launcher(QString expression, QString pathTo, QString pathFrom); //get input and run simplifier
  void runSimplifier(QString expression, QString pathTo); //get input and run simplifier. If pathTo is empty print response on console

};

#endif // MAINCLASS_H
