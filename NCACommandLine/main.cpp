﻿#include <QtCore/QCoreApplication>
#include <QDebug>
#include <QObject>

#include "mainclass.h"


int main(int argc, char *argv[]) //why ?
  {
  QCoreApplication a(argc, argv);
  a.setApplicationVersion(APP_CMD_VERSION);
  a.setApplicationName("NCA");

  MainClass mc;
  mc.setCommandsAndRun(a.arguments());

  //return a.exec();
  }
