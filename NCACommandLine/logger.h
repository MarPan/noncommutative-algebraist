﻿#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>

class Logger : public QObject
{
    Q_OBJECT
public:
    explicit Logger(QObject *parent = 0);

signals:

public slots:
  void message(QString& m);
  void message(QStringList& m);

};

#endif // LOGGER_H
