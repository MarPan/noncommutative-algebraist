#-------------------------------------------------
#
# Project created by QtCreator 2011-10-19T16:33:04
#
#-------------------------------------------------

QT       += core
QT       -= gui

INCLUDEPATH += ../libNCACore

CONFIG   += console
CONFIG   -= app_bundle

CONFIG(debug) {
    DEFINES += DEBUG
}

TEMPLATE = app

unix {
  TARGET = ../out/NCACommandLine
  target.path = /usr/bin
  INSTALLS += target
}
win32 {
  TARGET = ../../out/NCACommandLine
}
win64 {
  TARGET = ../../out/NCACommandLine
}

LIBS += -L../out -lNCACore

SOURCES += main.cpp \
    logger.cpp \
    mainclass.cpp

HEADERS += \
    logger.h \
    mainclass.h

QMAKE_LIBDIR += ../out

VERSION = 1.3.0
# Define the preprocessor macro to get the application version in our application.
DEFINES += APP_CMD_VERSION=\\\"$$VERSION\\\"
