#!/usr/bin/env python3
import glob, os, subprocess
from subprocess import PIPE

def runAndCaptureOutput(args, fallback = ""):
  # [:-1] drops the last character (\n).
  # stdout is of type 'bytes', that's why it needs decoding
  try:
    process = subprocess.run(args, stdout=PIPE, stderr=PIPE)
  except Exception as e:
    print("Exception thrown: %s" % str(e))
    return fallback

  return process.stdout.decode('utf-8')[:-1]


libFiles = glob.glob("out/libNCACore.so*")
fullLibName = max(libFiles, key=len)
version = fullLibName[len("out/libNCACore.so."):]

print("\nPackaging NCA version " + version + "\n")

# sth like /usr/bin
binariesInstallPath = runAndCaptureOutput(['systemd-path', 'system-binaries'], "/usr/bin") + "/"

# sth like /usr/lib; I appended /nca/ so I don't overwrite other possible Qt5 libs in there.
librariesInstallPath = runAndCaptureOutput(['systemd-path', 'system-library-private'], "/usr/lib") + "/nca/"

# sth like /usr/share
configInstallPath = runAndCaptureOutput(['systemd-path', 'system-shared'], "/usr/share") + "/nca/"

# sth like /usr/lib/x86_64-linux-gnu
qtLibsSource = runAndCaptureOutput(['qmake', '-query', 'QT_INSTALL_LIBS']) + "/"

# sth like /usr/lib/x86_64-linux-gnu/qt5/plugins
qtPluginsSource = runAndCaptureOutput(['qmake', '-query', 'QT_INSTALL_PLUGINS']) + "/platforms/"

# Prepare ld conf file
ncaConfFileName = "nca.conf"
postInstallScript = "ldconfig.sh"
print(librariesInstallPath, file=open(ncaConfFileName, "w"))
print("#!/bin/bash\nldconfig\necho \"Executed ldconfig\"\n", file=open(postInstallScript, "w"))

print("Will install to: \n" +
      "  " + binariesInstallPath + "\n" +
      "  " + librariesInstallPath + "\n" +
      "  " + configInstallPath + "\n" +
      "Qt libs folder: \n" +
      "  " + qtLibsSource + "\n")


fpmCommand = "fpm -s dir -t deb -n nca -v " + version + " -p nca-" + version + ".deb " + \
            "--after-install " + postInstallScript + " " + \
            "out/NCAGui=" + binariesInstallPath + " " +\
            "out/NCACommandLine=" + binariesInstallPath + " " +\
            "out/NCATests=" + binariesInstallPath + " " +\
            "config.xml=" + configInstallPath + " " + \
            "test_config.xml=" + configInstallPath + " " + \
            ncaConfFileName + "=" + "/etc/ld.so.conf.d/" + " "

# libNCACore.so* files
for str in libFiles:
  fpmCommand += str + "=" + librariesInstallPath + " "

# Qt libs required for deployment
qtLibs = ['libicudata.so.*', 'libicui18n.so.*', 'libicuuc.so.*',
          'libpng12.so.*', 'libQt5Core.so.*', 'libQt5DBus.so.*',
          'libQt5Gui.so.*', 'libQt5Test.so.*','libQt5Widgets.so.*',
          'libpcre2-16.so*', 'libdouble-conversion.so*', 'libglib-2.0.so*']
          
print("Adding qt libs:")
for qtLib in qtLibs:
  matchingFiles = glob.glob(qtLibsSource + qtLib)
  for file in matchingFiles:
    print("   " + file + " = " + librariesInstallPath)
    fpmCommand += file + "=" + librariesInstallPath + " "

qtPlugins = [ 'libqxcb.so' ]
for qtPlugin in qtPlugins:
  matchingFiles = glob.glob(qtPluginsSource + qtPlugin)
  for file in matchingFiles:
    print("   " + file + " = " + librariesInstallPath + "platforms/")
    fpmCommand += file + "=" + librariesInstallPath + "platforms/"



print("\nExecuting fpm command: \n\n" + fpmCommand + "\n")

for debFileStr in glob.glob("*deb"):
  os.remove(debFileStr)

process = subprocess.Popen(fpmCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()

print(error)
print(output)

