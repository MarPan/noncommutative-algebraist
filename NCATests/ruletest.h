#ifndef RULETEST_H
#define RULETEST_H

#include <QObject>
#include "core/translator.h"

class RuleTest : public QObject
  {
  Q_OBJECT
public:
  explicit RuleTest(QObject *parent = 0);
  Translator translator;

private slots:
  void initTestCase();
  void testComputeArgumentValue();
  void testComputeArgumentValue_data();
  void testCheckCondition();
  void testCheckCondition_data();
  void testCheckForDependencies();
  void testCheckForDependencies_data();
  void testFindPatternInExp();
  void testFindPatternInExp_data();
  void testGetLabelDictionary();
  void testGetLabelDictionary_data();
  void testApply();
  void testApply_data();
  void cleanupTestCase();
  };

#endif // RULETEST_H
