#include "config/groupgraph.h"
#include "groupgraphtest.h"

GroupGraphTest::GroupGraphTest(QObject *parent) :
  QObject(parent)
  {
  }

void GroupGraphTest::init()
  {
  gg = new GroupGraph();
  }

void GroupGraphTest::testAddGroup()
  {
  GroupLabel gl("name");
  gg->addGroup(gl);
  QCOMPARE(gg->size(), 1);
  gg->addGroup(gl);
  QCOMPARE(gg->size(), 1);
  GroupLabel gl_diff("different_name");
  gg->addGroup(gl_diff);
  QCOMPARE(gg->size(), 2);
  }

void GroupGraphTest::testRemoveGroup()
  {
  GroupLabel glA("A"), glB("B");
  //qDebug() << "&glA = " << &glA << "&glB= " << &glB;
  gg->addGroup(glA);
  gg->addGroup(glB);
  QCOMPARE(gg->size(), 2);

  gg->removeGroup(glA);
  QCOMPARE(gg->size(), 1);
  gg->removeGroup(glA);
  QCOMPARE(gg->size(), 1);
  gg->removeGroup(glB);
  QCOMPARE(gg->size(), 0);
  }


void GroupGraphTest::testAddSubgroup()
  {
  GroupLabel glA("A"), glB("B");
  gg->addGroup(glA);
  gg->addGroup(glB);
  QCOMPARE(gg->size(), 2);
  }

void GroupGraphTest::testRemoveSubgroup()
  {
  GroupLabel glA("A"), glB("B");
  gg->addGroup(glA);
  gg->addGroup(glB);
  gg->removeGroup(glA);
  QCOMPARE(gg->size(), 1);
  gg->removeGroup(glB);
  QCOMPARE(gg->size(), 0);
  }

void GroupGraphTest::cleanup()
  {
  delete gg;
  }
