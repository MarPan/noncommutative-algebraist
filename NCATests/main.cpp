#include <QtTest/QtTest>
#include <iostream>
#include "translatortest.h"
#include "expressionsimplifiertest.h"
#include "expressiontest.h"
#include "sentencetest.h"
#include "wordtest.h"
#include "configurationtest.h"
#include "ruletest.h"
#include "macroruletest.h"
#include "groupgraphtest.h"

int main(int argc, char *argv[])
  {
  Q_UNUSED(argc)
  Q_UNUSED(argv)

  QList<QObject*> tests{new TranslatorTest(),
                        new WordTest(),
                        new SentenceTest(),
                        new ExpressionTest(),
                        new RuleTest(),
                        new MacroRuleTest(),
                        new GroupGraphTest(),
                        new ConfigurationTest(),
                        new ExpressionSimplifierTest()};

  int failedCount = 0;

  foreach (QObject* qobj, tests)
    if (QTest::qExec(qobj))
      {
      failedCount++;
      std::cout << qobj->metaObject()->className() << " failed." << std::endl;
      }

  if (0 == failedCount)
    std::cout << std::endl << "All tests passed!" << std::endl << std::endl;
  else
    std::cout << std::endl  << failedCount << " tests failed!" << std::endl << std::endl;

  qDeleteAll(tests);
  tests.clear();
  return failedCount;
  }
