#include "wordtest.h"
#include "literals/delta.h"
#include "literals/operator.h"
#include "literals/variable.h"
#include "literals/sum.h"

WordTest::WordTest(QObject *parent) :
  QObject(parent)
  {
  }

void WordTest::initTestCase()
  {
  }

void WordTest::testIsGreaterThan()
  {
  Delta deltaXY("delta", "x", "y");
  Delta deltaAB("delta", "a", "b");

  QCOMPARE(deltaXY.isGreaterThan(deltaAB), true);
  QCOMPARE(deltaAB.isGreaterThan(deltaXY), false);

  Variable a("a"), aa("aa"), z("z");

  QCOMPARE(aa.isGreaterThan(a), true);
  QCOMPARE(aa.isGreaterThan(z), false);
  QCOMPARE(a.isGreaterThan(aa), false);
  QCOMPARE(a.isGreaterThan(z), false);
  QCOMPARE(z.isGreaterThan(a), true);
  QCOMPARE(z.isGreaterThan(aa), true);

  QCOMPARE(aa.isGreaterThan(a), (a.getText().compare( aa.getText()) < 0));
  QCOMPARE(aa.isGreaterThan(z), (z.getText().compare( aa.getText()) < 0));
  QCOMPARE(a.isGreaterThan(aa), (aa.getText().compare( a.getText()) < 0));
  QCOMPARE(a.isGreaterThan(z), (z.getText().compare( a.getText()) < 0));
  QCOMPARE(z.isGreaterThan(a), (a.getText().compare( z.getText()) < 0));
  QCOMPARE(z.isGreaterThan(aa), (aa.getText().compare( z.getText()) < 0));
  }

void WordTest::testMergeWithInnerSum()
  {
  Sum* innerSum = new Sum(R"(\SUM)", QStringList("innerSum"));
  innerSum->append(new Delta("delta", "arg1", "arg2"));
  innerSum->append(new Variable("variable"));
  innerSum->append(new Operator("boson_creation", "label"));


  Sum outerSum(R"(\SUM)", QStringList("outerSum"));
  outerSum.append(new Operator("operator", "front"));
  outerSum.append(innerSum);
  outerSum.append(new Operator("operator", "back"));

  outerSum.mergeWithInnerSum();

  QCOMPARE(outerSum.size(), 5);
  QCOMPARE(outerSum.at(0)->getType(), Word::WT_OPERATOR);
  QCOMPARE(outerSum.at(1)->getType(), Word::WT_DELTA);
  QCOMPARE(outerSum.at(2)->getType(), Word::WT_VARIABLE);
  QCOMPARE(outerSum.at(3)->getType(), Word::WT_OPERATOR);
  QCOMPARE(outerSum.at(4)->getType(), Word::WT_OPERATOR);

  }

void WordTest::cleanupTestCase()
  {  }

