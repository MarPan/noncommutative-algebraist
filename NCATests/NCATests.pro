#-------------------------------------------------
#
# Project created by QtCreator 2013-11-08T20:59:12
#
#-------------------------------------------------

QT       += testlib
QT       -= gui

INCLUDEPATH += ../libNCACore

CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

unix {
  TARGET = ../out/NCATests
  target.path = /usr/bin
  INSTALLS += target
}

_GITLAB = $$(GITLAB)
equals( _GITLAB, "1") {
  DEFINES += GITLAB
}

win32 {
  TARGET = ../../out/NCATests
}
win64 {
  TARGET = ../../out/NCATests
}

SOURCES += main.cpp \
    translatortest.cpp \
    expressiontest.cpp \
    expressionsimplifiertest.cpp \
    sentencetest.cpp \
    wordtest.cpp \
    configurationtest.cpp \
    ruletest.cpp \
    groupgraphtest.cpp \
    macroruletest.cpp

HEADERS += \
    translatortest.h \
    expressiontest.h \
    expressionsimplifiertest.h \
    sentencetest.h \
    wordtest.h \
    configurationtest.h \
    ruletest.h \
    groupgraphtest.h \
    macroruletest.h

LIBS += -L../out -lNCACore

OBJECTS_DIR = ../intermediate/cl/
MOC_DIR = ../intermediate/cl/
