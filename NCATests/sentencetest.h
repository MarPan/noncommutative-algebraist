#ifndef SENTENCETEST_H
#define SENTENCETEST_H

#include <QtTest/QtTest>
#include "core/translator.h"
class Sentence;

class SentenceTest : public QObject
{
    Q_OBJECT
public:
    explicit SentenceTest(QObject *parent = 0);

private:
  Sentence *rsentence;
  Sentence *lsentence;
  Translator translator;

private slots:
    void initTestCase();
    void testComparison();
    void testComparison_data();
    void testBasicConcatenate();
    void testBasicConcatenate_data();
    void testSortDeltas();
    void testSortDeltas_data();
    void testSortVariables();
    void testSortVariables_data();
    void testSortFunctions();
    void testSortFunctions_data();
    void testSortAllTypesInSentence();
    void testSortAllTypesInSentence_data();
    void testDoDeltaTrick();
    void testDoDeltaTrick_data();
    void testGetCommonPart();
    void testGetCommonPart_data();
    void testSimplifySums();
    void testSimplifySums_data();
    void cleanupTestCase();
};

#endif // SENTENCETEST_H
