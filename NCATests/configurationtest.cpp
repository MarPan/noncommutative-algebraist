#include "configurationtest.h"
#include "config/configuration.h"

ConfigurationTest::ConfigurationTest(QObject *parent) :
  QObject(parent),
  configuration(nullptr),
#ifdef GITLAB
  _configFilePath("/usr/share/nca/test_config.xml")
#else
  _configFilePath("../test_config.xml")
#endif
  {
  }

void ConfigurationTest::initTestCase()
  {
  configuration = new Configuration(translator);
  }

void ConfigurationTest::testLoadXML()
  {
  QVERIFY(configuration->loadXML(_configFilePath));
  QCOMPARE(configuration->rules().count(), 23);
  QCOMPARE(configuration->macroRules().count(), 1);
  QCOMPARE(translator.clearRegisteredNames(), 5);
  }

void ConfigurationTest::testUpdateMacroRules()
  {
  // we need to run updateMacroRules
  // and than see if the rules in _generatedRules are ok.
  // we can simply check the number. We check the validity of the rules in
  // macrorulestest.cpp
  configuration->loadXML(_configFilePath);
  Expression exp;
  translator.stringToExp("a_{i j} b_{x y}", exp);
  configuration->updateMacroRules(exp);
  QCOMPARE(configuration->_generatedRules.size(), 2);
  }

// called after the last testfunction was executed
void ConfigurationTest::cleanupTestCase()
  {
  delete configuration;
  }
