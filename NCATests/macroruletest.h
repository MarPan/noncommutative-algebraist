#ifndef MACRORULETEST_H
#define MACRORULETEST_H

#include <QObject>
#include "core/translator.h"

class MacroRuleTest : public QObject
  {
  Q_OBJECT
public:
  explicit MacroRuleTest(QObject *parent = 0);

  Translator translator;

signals:

private slots:
  void initTestCase();
  void testGetNextCombination();
  void testGetNextCombination_data();
  void testGenerateRules();
  void testGenerateRules_data();
  void cleanupTestCase();
  };

#endif // MACRORULETEST_H
