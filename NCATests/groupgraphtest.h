#ifndef GROUPGRAPHTEST_H
#define GROUPGRAPHTEST_H

#include <QtTest/QtTest>
class GroupGraph;


class GroupGraphTest : public QObject
  {
  Q_OBJECT
public:
  explicit GroupGraphTest(QObject *parent = 0);

  // maybe some predefined gls too?
  GroupGraph *gg;

signals:

private slots:
  void init();
  void testAddGroup();
  void testRemoveGroup();
  void testAddSubgroup();
  void testRemoveSubgroup();
  void cleanup();

  };

#endif // GROUPGRAPHTEST_H
