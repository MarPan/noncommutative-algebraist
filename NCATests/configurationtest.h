#ifndef CONFIGURATIONTEST_H
#define CONFIGURATIONTEST_H

#include <QtTest/QtTest>
#include "core/translator.h"
class Configuration;

class ConfigurationTest : public QObject
  {
  Q_OBJECT
public:
  explicit ConfigurationTest(QObject *parent = nullptr);

private:
  Configuration *configuration;
  Translator translator;

private slots:
  void initTestCase();
  void testLoadXML();
  void testUpdateMacroRules();
  void cleanupTestCase();

private:
  QString _configFilePath;
  };

#endif // CONFIGURATIONTEST_H
