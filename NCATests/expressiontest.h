#ifndef EXPRESSIONTEST_H
#define EXPRESSIONTEST_H

#include <QtTest/QtTest>
#include "core/translator.h"
class Expression;

class ExpressionTest : public QObject
  {
  Q_OBJECT
public:
  explicit ExpressionTest(QObject *parent = nullptr);

private:
  Expression* expression;
  Translator translator;

private slots:
  void initTestCase();
  void testMultiplication();
  void testMultiplication_data();
  void testSort();
  void testSort_data();
  void testFactorOutCommonSentence();
  void testFactorOutCommonSentence_data();
  void testFactorOutType();
  void testFactorOutType_data();
  void testFactorOutWhatYouCan();
  void testFactorOutWhatYouCan_data();
  void testApplyLabelDictionary();
  void testApplyLabelDictionary_data();
  void testRemoveWords();
  void testRemoveWords_data();
  void cleanupTestCase();

  };

#endif // EXPRESSIONTEST_H
