#ifndef WORDTEST_H
#define WORDTEST_H

#include <QtTest/QtTest>

class WordTest : public QObject
  {
  Q_OBJECT
public:
  explicit WordTest(QObject *parent = nullptr);

signals:

private slots:
  void initTestCase();
  void testIsGreaterThan();
  void testMergeWithInnerSum();
  void cleanupTestCase();
};

#endif // WORDTEST_H
