#ifndef EXPRESSIONSIMPLIFIERTEST_H
#define EXPRESSIONSIMPLIFIERTEST_H

#include <QtTest/QtTest>
class ExpressionSimplifier;

class ExpressionSimplifierTest : public QObject
  { 
  Q_OBJECT
public:
  explicit ExpressionSimplifierTest(QObject *parent = nullptr);

private:
  ExpressionSimplifier *expressionSimplifier;

private slots:
  void initTestCase();
  void testDoOneStep();
  void testDoOneStep_data();
  void testBasicSimplifying();
  void testBasicSimplifying_data();
  void testGetRidOfBrackets();
  void testGetRidOfBrackets_data();
  void testSimplifyDeltas();
  void testSimplifyDeltas_data();
  void testSameSentencesArithmetic();
  void testSameSentencesArithmetic_data();

  void benchmarkSimplifying();

  void cleanupTestCase();
  };

#endif //EXPRESSIONSIMPLIFIERTEST_H
