#include "expressionsimplifiertest.h"
#include "core/expressionsimplifier.h"

ExpressionSimplifierTest::ExpressionSimplifierTest(QObject *parent) :
    QObject(parent),
    expressionSimplifier(nullptr)
  {
  }

void ExpressionSimplifierTest::initTestCase()
  {
#ifdef GITLAB
  expressionSimplifier = new ExpressionSimplifier(true, "/usr/share/nca/test_config.xml");
#else
  expressionSimplifier = new ExpressionSimplifier(true, "../test_config.xml");
#endif
  }

void ExpressionSimplifierTest::testDoOneStep_data()
  {
  QTest::addColumn<QString>("input");
  QTest::addColumn<QString>("result");

  QTest::newRow("1") << "a_x b_y a_z" << "((delta(x,y)) - b_y a_x) a_z";
  QTest::newRow("2") << "delta(m,n) + a_x b_y" << "delta(m,n) + ((delta(x,y)) - b_y a_x)";
  QTest::newRow("3") << "a_x b_y a_z" << "((delta(x,y)) - b_y a_x) a_z";
  QTest::newRow("4") << "a_{k l} b_{i j}" << "((delta(i,k) + delta(j,l)) - b_{i j} a_{k l})";  // tests macro
  QTest::newRow("5") << "a_k b_i" << "((delta(i,k)) - b_i a_k)";  // tests macro
  }

void ExpressionSimplifierTest::testDoOneStep()
  {
  QFETCH(QString, input);
  QFETCH(QString, result);

  expressionSimplifier->setExpFromString( input );
  expressionSimplifier->doOneStep();

  QCOMPARE(expressionSimplifier->getExp(), result);
  }

void ExpressionSimplifierTest::testBasicSimplifying_data()
  {
  QTest::addColumn<QString>("input");
  QTest::addColumn<QString>("result");

  QTest::newRow("1") << "a_i b_j" << "-b_j a_i + delta(i,j)";
  QTest::newRow("2") << "3 b_j a_i" << "3 b_j a_i";
  QTest::newRow("3") << "a_x b_y + delta(m,n)" << "-b_y a_x + delta(m,n) + delta(x,y)";
  QTest::newRow("4") << "b_x a_y + delta(m,n)" << "b_x a_y + delta(m,n)";
  QTest::newRow("5") << "delta(m,n) + b_x a_y" << "b_x a_y + delta(m,n)";
  QTest::newRow("6") << "delta(m,n) + a_x b_y" << "-b_y a_x + delta(m,n) + delta(x,y)";
  QTest::newRow("7") << "a_x b_y a_z" << "-b_y a_x a_z + delta(x,y) a_z";
  QTest::newRow("8") << "b_x a_y b_z" << "-b_x b_z a_y + delta(y,z) b_x";
  QTest::newRow("9") << "-(b_x a_y + a_z) delta(m,n)" << "-delta(m,n) (b_x a_y + a_z)";
  QTest::newRow("10") << "1" << "1";
  QTest::newRow("11") << "(a_i) b_j" << "-b_j a_i + delta(i,j)";
  QTest::newRow("12") << "delta(x,y) + delta(m,n)" << "delta(m,n) + delta(x,y)";
  QTest::newRow("13") << "c_3 c_1" << "c_1 c_3";
  }

void ExpressionSimplifierTest::testBasicSimplifying()
  {
  QFETCH(QString, input);
  QFETCH(QString, result);

  expressionSimplifier->setExpFromString(input);
  QStringList list = expressionSimplifier->simplify();

  QCOMPARE(expressionSimplifier->getExp(), result);
  }

void ExpressionSimplifierTest::benchmarkSimplifying()
  {
  QStringList complexCases = { "a_x b_y a_z", "b_x a_y b_z"};
  QString input;

  QBENCHMARK
    {
    for (int i = 0; i < 100; i++)
      foreach (input, complexCases)
        {
        expressionSimplifier->setExpFromString(input);
        QStringList list = expressionSimplifier->simplify();
        }
    }
  }

void ExpressionSimplifierTest::testGetRidOfBrackets_data()
  {
  QTest::addColumn<QString>("input");
  QTest::addColumn<QString>("result");

  QTest::newRow("1") << "(a_i b_j)" << "a_i b_j";
  QTest::newRow("2") << "(b_j a_i) 3 a_z" << "3 b_j a_i a_z";
  QTest::newRow("3") << "3 a_z (b_j a_i)" << "3 a_z b_j a_i";
  QTest::newRow("4") << "3 a_z (b_j a_i) delta(x,y)" << "3 a_z b_j a_i delta(x,y)";
  QTest::newRow("5") << "-(b_x a_y + a_z) delta(m,n)" << "-b_x a_y delta(m,n) - a_z delta(m,n)";
  }

void ExpressionSimplifierTest::testGetRidOfBrackets()
  {
  QFETCH(QString, input);
  QFETCH(QString, result);

  expressionSimplifier->setExpFromString( input );
  expressionSimplifier->multiplyAllBrackets();

  QCOMPARE(expressionSimplifier->getExp(), result);
  }

void ExpressionSimplifierTest::testSimplifyDeltas_data()
  {
  QTest::addColumn<QString>("input");
  QTest::addColumn<QString>("result");

  QTest::newRow("1") << "delta(2,3) + a_1" << "a_1";
  QTest::newRow("2") << "delta(3,3) + a_1" << "1 + a_1";
  QTest::newRow("3") << "delta(m,n) + delta(x,y) - b_y a_x" << "delta(m,n) + delta(x,y) - b_y a_x";
  QTest::newRow("4") << "delta(3,4) + delta(3,3) - b_y a_x" << "1 - b_y a_x";
  QTest::newRow("5") << "a_i delta(2,2) x f(y)" << "x f(y) a_i";
  }

void ExpressionSimplifierTest::testSimplifyDeltas()
  {
  QFETCH(QString, input);
  QFETCH(QString, result);

  expressionSimplifier->setExpFromString( input );
  expressionSimplifier->simplifyDeltas();

  QCOMPARE(expressionSimplifier->getExp(), result);
  }

void ExpressionSimplifierTest::testSameSentencesArithmetic_data()
  {
  QTest::addColumn<QString>("input");
  QTest::addColumn<QString>("result");

  QTest::newRow("1") << "delta(2,3) + delta(2,3)" << "2 delta(2,3)";
  QTest::newRow("2") << "1.1 + 2.2 + 3.3 + 4.4" << "11";
  QTest::newRow("3") << "delta(m,n) - delta(m,n)" << "0";
  QTest::newRow("4") << "a_1 - a_1 + 1" << "1";
  QTest::newRow("5") << "a_i delta(2,2) x f(y) - x f(y)" << "a_i delta(2,2) x f(y) - x f(y)";
  QTest::newRow("6") << "2 a_i - x f(y) - 3.3 a_i" << "-1.3 a_i - x f(y)";
  QTest::newRow("7") << "2 a_i b_j + a_i" << "2 a_i b_j + a_i";
  QTest::newRow("8") << "2 a_i + a_i b_j" << "2 a_i + a_i b_j";
  }

void ExpressionSimplifierTest::testSameSentencesArithmetic()
  {
  QFETCH(QString, input);
  QFETCH(QString, result);

  expressionSimplifier->setExpFromString( input );
  expressionSimplifier->sameSentencesArithmetic();

  QCOMPARE(expressionSimplifier->getExp(), result);
  }

void ExpressionSimplifierTest::cleanupTestCase()
  {
  delete expressionSimplifier;
  }
