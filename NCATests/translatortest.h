#ifndef TRANSLATORTEST_H
#define TRANSLATORTEST_H

#include <QtTest/QtTest>
class Translator;
class Expression;

class TranslatorTest : public QObject
  { 
  Q_OBJECT
public:
  explicit TranslatorTest(QObject *parent = nullptr);

private:
  Translator *translator;
  Expression *expression;

private slots:
  void initTestCase();  
  void cleanupTestCase();
  void testSameInSameOut();  
  void testSameInSameOut_data();
  void testStringToSentence();
  void testStringToSentence_data();
  void testExpToString();
  void testExpToString_data();
  void testIdioticInput_data();
  void testIdioticInput();
  };

#endif // TRANSLATORTEST_H
