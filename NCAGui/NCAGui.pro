#-------------------------------------------------
#
# Project created by QtCreator 2012-02-18T10:11:47
#
#-------------------------------------------------

QT += core widgets

INCLUDEPATH += ../libNCACore/

CONFIG(debug) {
    DEFINES += DEBUG
}

TEMPLATE = app

unix {
  TARGET = ../out/NCAGui
  target.path = /usr/bin
  INSTALLS += target
}
win32 {
  TARGET = ../../out/NCAGui
}
win64 {
  TARGET = ../../out/NCAGui
}

LIBS += -L../out -lNCACore

SOURCES += main.cpp\
        mainwindow.cpp \

HEADERS  += mainwindow.h \

FORMS    += mainwindow.ui

OBJECTS_DIR = ../intermediate/gui/
MOC_DIR = ../intermediate/gui/
