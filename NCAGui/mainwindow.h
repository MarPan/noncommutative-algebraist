﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include "core/translator.h"

class ExpressionSimplifier;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    ExpressionSimplifier *_simplifier;

public slots:
    void on_pbParse_clicked();
    void on_pbLoadConfig_clicked();
    void on_pbClearLog_clicked();
    void on_txtExpression_textChanged();
    void showError(QString);
    void clearStatusBarColor();
};

#endif // MAINWINDOW_H
